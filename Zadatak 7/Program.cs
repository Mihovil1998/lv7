﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD movie = new DVD("akcijski film", DVDType.MOVIE, 21.5);
            VHS animatedmovie = new VHS("crtani film", 13.5);
            Book book = new Book("knjiga", 24.5);
            DVD software = new DVD("software", DVDType.SOFTWARE, 26.5);
            RentVisitor rentVisitor = new RentVisitor();
            Cart cart = new Cart();
            cart.Add(movie);
            cart.Add(animatedmovie);
            cart.Add(book);
            cart.Add(software);
            Console.WriteLine(cart.Accept(rentVisitor));
        }
    }
}
