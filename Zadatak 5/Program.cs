﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD movie = new DVD("akcijski film", DVDType.MOVIE, 21.5);
            VHS animatedmovie = new VHS("crtani film", 13.5);
            Book book = new Book("knjiga", 24.5);
            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine(movie.Accept(buyVisitor));
            Console.WriteLine(animatedmovie.Accept(buyVisitor));
            Console.WriteLine(book.Accept(buyVisitor));
        }
    }
}
