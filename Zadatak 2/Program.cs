﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 8.4, 3.5, 2, 10.1, 25, 38, 7.5, 52 };
            NumberSequence numberSequence = new NumberSequence(array);
            LinearSearch linearSearch = new LinearSearch();
            numberSequence.SetSearchStrategy(linearSearch);
            int index = numberSequence.Search(25);
            if (index >= 0)
                Console.WriteLine("Found at: " + index + "!");
            else
                Console.WriteLine("Not found!");
        }
    }
}
